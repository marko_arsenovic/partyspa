import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import promise from "redux-promise";

import reducers from "./reducers";
import App from "./components/app";


import { composeWithDevTools } from 'redux-devtools-extension';

const composeEnhancers = composeWithDevTools({
  // specify name here, actionsBlacklist, actionsCreators and other options if needed
});
const store = createStore(reducers, /* preloadedState, */ composeEnhancers(
  applyMiddleware(promise),
  // other store enhancers if any
));

ReactDOM.render(
  //we can place BrowserRouter for routing here...
  <Provider store={store}>
    <App/>
  </Provider>,
  document.querySelector(".container")
);
