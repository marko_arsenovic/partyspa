import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
//import reducers...
import MockReducer from "./mock_reducer";

const rootReducer = combineReducers({
  mock: MockReducer,
});

export default rootReducer;
