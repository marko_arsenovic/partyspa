import _ from "lodash";
import { MOCK_ACTION } from "../actions";

export default function(state = {}, action) {
  switch (action.type) {
    case MOCK_ACTION:
      return "MOCK";
    default:
      return state;
  }
}