import axios from "axios";

export const MOCK_ACTION = "MOCK_ACTION";


export function mockAction(obj) {

  return {
    type: "MOCK_ACTION",
    payload: obj
  };
}
